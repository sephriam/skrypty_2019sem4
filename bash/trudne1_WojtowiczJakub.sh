#!/bin/bash

declare -A WORDS

if [ $# -lt 1 ]; then
    echo "Usage: "
    echo $0 "<input file>"
    exit 1
fi

I=0
while IFS= read -r LINE
do
    WORD_IN_LINE=$(echo $LINE | sed -E -e 's/[[:blank:]]+/\n/g')
    for WORD in ${WORD_IN_LINE[@]}; do
        WORDS["$WORD"]="${WORDS["$WORD"]} $I"
    done
    I=$(($I+1))

done < $1

for KEY in ${!WORDS[@]}; do
    echo "$KEY${WORDS[$KEY]}"
done | sort


