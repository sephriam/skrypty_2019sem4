#!/bin/bash

if [ $# -lt 1 ]; then
    echo "Usage: "
    echo $0 "<input file>"
    exit 1
fi

sed -i -e 's/[^$]#[^!].*//g;s/^#[^!].*//g' $1

