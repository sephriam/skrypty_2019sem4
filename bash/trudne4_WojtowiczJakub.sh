#!/bin/bash

if [ $# -lt 1 ] || [ $# -gt 10 ]; then
    echo "This script can take up to 10 arguments"
    echo "Usage: "
    echo $0 "<arguments ...>"
    exit 1
fi

for WORD in $@; do

    if [ ${#WORD} -gt 10 ]; then
        echo "Word ${WORD} is too long" > /dev/stderr
    fi
done

randomChar() {
    echo $(cat /dev/urandom | tr -dc 'A-Z' | fold -w 1 | head -n 1)
}

printLine() {
    I=0
    WORD=$1
    while [ $I -lt $(((10 - ${#WORD})/2)) ]; do
        echo -n "$(randomChar) "
        I=$(($I+1))
    done

    echo -n $WORD | tr a-z A-Z | sed -rne 's/(.)/\1 /pg'

    I=$(($I+${#WORD}))
    while [ $I -lt 10 ]; do
        echo -n "$(randomChar) "
        I=$(($I+1))
    done

    echo ""
    LINES=$(($LINES+1))
}

LINES=0
for WORD in $@; do

    if [ ${#WORD} -gt 10 ]; then
        WORD=""
    fi

    printLine ${WORD}

    if [ $LINES -eq 10 ]; then
        break
    fi
done

while [ $LINES -lt 10 ]; do
    printLine ${WORD}
done