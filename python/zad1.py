def sumDigits(value):
    sum = 0
    while value > 0:
        sum += value % 10
        value //= 10
    return sum

def isPrime(value):
    if value == 0 or value == 1:
        return False
    for i in range(2, value - 1):
        if value % i == 0:
            return False
    return True

def findPrimes(begin, end, filepath):
    f = open(filepath, "w")
    for i in range(begin, end):
        if isPrime(sumDigits(i)):
            f.write(str(i) + "\r\n")
    f.close()


findPrimes(0, 200, "res.txt")