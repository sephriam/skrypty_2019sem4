import os

def findUniqueFileNames(dirPath):
    dict = {}
    for subdir, dirs, files in os.walk(dirPath):
        for file in files:
            if file in dict:
                dict[file] += 1
            else:
                dict[file] = 1
    uniqueList = []
    for key, value in dict.items():
        if value == 1:
            uniqueList.append(key)
    return uniqueList

print(findUniqueFileNames(".."))
print(findUniqueFileNames("/home/wojtowic/workspace/sem4/skrypty/python/justfortests"))