import datetime
import sys

def printCalendar(date):
    print("Pn\tWt\tSr\tCz\tPt\tSo\tNd")
    for i in range(date.weekday()):
        print("\t", end = "")
    print(str(date.day), end = "")
    date += datetime.timedelta(days=1)
    while(date.weekday() != 0):
        print("\t" + str(date.day), end = "")
        date += datetime.timedelta(days=1)
    print()
    month = date.month
    while date.month == month:
        print(str(date.day) + "\t", end = "")
        date += datetime.timedelta(days=1)
        if date.weekday() == 0:
            print()
        
monthMap = {"styczen": 1, "luty": 2, "marzec": 3, "kwiecien": 4, "maj": 5, "czerwiec": 6,
"lipiec": 7, "sierpien": 8, "wrzesien": 9, "pazdziernik": 10, "listopad": 11, "grudzien": 12}

if len(sys.argv) < 3:
    print("I need at least 2 arguments\r\n   ", sys.argv[0], "<year> <month>")
    exit(0)
elif str(sys.argv[2]) in monthMap:
    date = datetime.datetime(int(sys.argv[1]), monthMap[str(sys.argv[2])], 1)
    printCalendar(date)
else:
    print("Specified month is incorrect")

print()