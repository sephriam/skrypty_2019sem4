

def count(string):
    dict = {}
    for char in string:
        if char in dict:
            dict[char] += 1
        else:
            dict[char] = 1
    return sorted(dict.items(), reverse=True, key=lambda x: x[1])


for item in count("muahahahaxaxa"):
    if item[1] > 1:
        print(item[0], "-" , item[1])