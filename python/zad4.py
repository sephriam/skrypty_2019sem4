import re

def search(text):
    return re.search('(\+?[0-9 \-]{7,15})', text).group(0)


print(search("22 7777777"))
print(search("+48 123-234 567 xaxaxa"))