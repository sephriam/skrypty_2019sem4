from datetime import datetime

def diff(d1, d2):
    d1 = datetime.strptime(d1, "%d.%m.%Y")
    d2 = datetime.strptime(d2, "%d.%m.%Y")
    return abs((d2 - d1).days)

print(diff("11.09.2017", "30.09.2017"))
print(diff("1.01.2001", "1.01.2003"))
print(diff("1.01.2000", "1.01.2002"))