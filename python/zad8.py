def countAndAppend(*argv):
    for arg in argv:
        dict = {}
        file = open(arg, "r+")
        for line in file.readlines():
            for string in line.split():
                if string in dict:
                    dict[string] += 1
                else:
                    dict[string] = 1
        tuples = sorted(dict.items(), reverse=True, key=lambda x: x[1])
        for i in range(10):
            file.write(str(tuples[i][0]) + " - " + str(tuples[i][1]) + "\r\n")
        file.close()

countAndAppend("res.txt", "res.txt", "res.txt", "res.txt", "res.txt", "res.txt")