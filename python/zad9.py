import mmap
import os

def findInFile(string, filePath):
    f = open(filePath)
    s = mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)
    if s.find(bytes(string, "utf-8")) != -1:
        return True

def findInDir(string, dirPath):
    fileList = []
    for filename in os.listdir(dirPath):
        if os.path.isfile(os.path.join(dirPath, filename)): 
            if findInFile(string, os.path.join(dirPath, filename)):
                fileList.append(filename)
    return fileList


print("find in current directory")
print(findInDir("find", "."))

print("def in current /home/wojtowic/workspace/sem4/skrypty/python")
print(findInDir("def", "/home/wojtowic/workspace/sem4/skrypty/python"))
